package com.lk.thread;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class Test01 {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        MyThread myThread = new MyThread();
        Thread myRunnable = new Thread(new MyRunnable());

        FutureTask task = new FutureTask(new MyCallable());
        Thread myCallable = new Thread(task);


        myThread.start();
        myRunnable.start();
        myCallable.start();
        System.out.println(task.get());

    }
}
