package com.lk.thread;

import com.sun.org.apache.bcel.internal.classfile.LocalVariable;

public class ThreadLocalTest {

    //1.print函数
    static void print(String str){
        //1.1 打印当前线程本地内存中localVariable变量的值
        System.out.println(str + ":" + localVariable.get());
        //1.2 清除当前线程本地内存中的localVariable变量
        localVariable.remove();
    }

    //2.创建ThreadLocal变量
    static ThreadLocal<String> localVariable= new ThreadLocal<>();

    public static void main(String[] args) {
        Thread threadOne = new Thread(()->{
            //3.1 设置线程One中本地变量localVariable的值
            localVariable.set("threadOne local variable");
            //3.2调用打印函数
            print("threadOne");
            //3.3 打印本地变量
            System.out.println("threadOne remove after" + ":" + localVariable.get());
        });
        Thread threadTwo = new Thread(()->{
            //3.1 设置线程One中本地变量localVariable的值
            localVariable.set("threadTwo local variable");
            //3.2调用打印函数
            print("threadTwo");
            //3.3 打印本地变量
            System.out.println("threadTwo remove after" + ":" + localVariable.get());
        });

        threadOne.start();
        threadTwo.start();
    }
}
