package com.lk.thread;

import sun.misc.Unsafe;

import java.lang.reflect.Field;

public class TestUnSafe {
    //获取Unsafe的实例
    static final Unsafe unsafe;

    //记录变量state在类TestUnsafe中的偏移量
    static final long stateOffset;

    private volatile long state = 0;

    static{
        try{
            //利用反射获取Unsafe的成员变量theUnsafe
            Field field = Unsafe.class.getDeclaredField("theUnsafe");
            //设置为可存取
            field.setAccessible(true);
            //获取该变量的值
            unsafe = (Unsafe)field.get(null);
            //获取state变量在类TestUnSafe中的偏移量
            stateOffset = unsafe.objectFieldOffset(TestUnSafe.class.getDeclaredField("state"));
        }catch (Exception e){
            System.out.println(e.getLocalizedMessage());
            throw new Error(e);
        }
    }

    public static void main(String[] args) {
        //创建实例,并设置state值为1
        TestUnSafe test = new TestUnSafe();

        Boolean success = unsafe.compareAndSwapInt(test, stateOffset, 0 ,1);

        System.out.println(success);
    }
}
