package com.lk.juc;

import java.util.concurrent.locks.LockSupport;

public class LockSupportTest02 {

    public static void main(String[] args) throws InterruptedException {
        String a = new String("A");
        Thread t = new Thread(()->{
            System.out.println("睡觉");
            LockSupport.park(a);
            System.out.println("起床");
            System.out.println("是否中断:" + Thread.currentThread().isInterrupted());
        });
        t.setName("A-Name");
        t.start();
        t.interrupt();
        System.out.println("1222----1111");
    }
}
