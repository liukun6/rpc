package com.lk.juc;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Test01 {
    public static void main(String[] args) throws InterruptedException {
        final int theadSize = 1000;
        ThreadUnsafeExample threadUnsafeExample = new ThreadUnsafeExample();
        final CountDownLatch countDownLatch = new CountDownLatch(theadSize);

        ExecutorService executorService = Executors.newCachedThreadPool();

        for (int i = 0; i < theadSize; i++) {
            executorService.execute(()->{
                threadUnsafeExample.add();;
                countDownLatch.countDown();
            });
        }
        countDownLatch.await();
        executorService.shutdown();;
        System.out.println(threadUnsafeExample.get());
    }
}
