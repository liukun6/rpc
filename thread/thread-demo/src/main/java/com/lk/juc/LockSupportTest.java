package com.lk.juc;

import java.util.concurrent.locks.LockSupport;

public class LockSupportTest {
    public static void main(String[] args) {
        Thread t1 = new Thread(()->{
            System.out.println(Thread.currentThread().getName() + "进入线程");
            LockSupport.park();
            System.out.println("t1线程运行结束");
        });
        t1.start();
        System.out.println("t1已经启动,但是在内部进行了park");
        LockSupport.unpark(t1);
        System.out.println("LockSupport进行了unpark");
    }
}
