package com.lk.search.test;

import com.lk.search.pojo.News;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.wltea.analyzer.lucene.IKAnalyzer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

public class CreateIndex {
    public static void main(String[] args) {
        News new1 = new News();

        new1.setId(1);
        new1.setTitle("习近平会见美国总体奥巴马,学习国外经验");
        new1.setContent("国家主席习近平9月3日在杭州西湖国宾馆会见前来出席二十国集团领导人杭州峰会的美国总体奥巴马...");
        new1.setReply(672);

        News new2 = new News();
        new1.setId(2);
        new1.setTitle("北大迎4380名新生,农村学生700多人近年最多");
        new1.setContent("昨天,北京大学迎来4380名来自全国各地及数十个国家的本科生。其中,农村学生共700余名,为近年最多....");
        new1.setReply(995);

        News new3 = new News();
        new1.setId(3);
        new1.setTitle("特朗普宣誓(Donald Trump) 就任美国第45任总统");
        new1.setContent("当地时间1月20日,唐纳德.特朗普在美国国会宣誓就职,正式成为美国第45任总统。");
        new1.setReply(1872);

        //创建IK分词器
        Analyzer analyzer = new IKAnalyzer();

        IndexWriterConfig icw = new IndexWriterConfig();
        icw.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
        Directory directory = null;
        IndexWriter indexWriter = null;
        //索引目录
        Path indexPath = Paths.get("indexdir");
        //开始时间
        Date start = new Date();
        try {
            if(Files.isReadable(indexPath)) {
                System.out.println("Document directory' " + indexPath.toAbsolutePath() + " ' does not exist or is not readable, please check the path");
                System.exit(1);
            }
            directory = FSDirectory.open(indexPath);
            indexWriter = new IndexWriter(directory, icw);
            //设置新闻ID索引并存储
            FieldType idType = new FieldType();
            idType.setIndexOptions(IndexOptions.DOCS);
            idType.setStored(true);

            //设置新闻标题索引文档、词项频率、位移信息和偏移量,存储并词条化
            FieldType titleType = new FieldType();
            titleType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
            titleType.setStored(true);
            titleType.setTokenized(true);
            FieldType contentType = new FieldType();
            contentType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
            contentType.setStored(true);
            contentType.setTokenized(true);
            contentType.setStoreTermVectors(true);
            contentType.setStoreTermVectorPositions(true);
            contentType.setStoreTermVectorOffsets(true);
            contentType.setStoreTermVectorPayloads(true);
            Document doc1 = new Document();
            doc1.add(new Field("id", String.valueOf(new1.getId()), idType));
            doc1.add(new Field("title", String.valueOf(new1.getTitle()), titleType));
            doc1.add(new Field("content", String.valueOf(new1.getContent()), contentType));
            doc1.add(new IntPoint("reply", new1.getReply()));
            doc1.add(new StoredField("reply_display", new1.getReply()));
            Document doc2 = new Document();
            doc2.add(new Field("id", String.valueOf(new2.getId()), idType));
            doc2.add(new Field("title", String.valueOf(new2.getTitle()), titleType));
            doc2.add(new Field("content", String.valueOf(new2.getContent()), contentType));
            doc2.add(new IntPoint("reply", new2.getReply()));
            doc2.add(new StoredField("reply_display", new2.getReply()));
            Document doc3 = new Document();
            doc3.add(new Field("id", String.valueOf(new3.getId()), idType));
            doc3.add(new Field("title", String.valueOf(new3.getTitle()), titleType));
            doc3.add(new Field("content", String.valueOf(new3.getContent()), contentType));
            doc3.add(new IntPoint("reply", new3.getReply()));
            doc3.add(new StoredField("reply_display", new3.getReply()));
            indexWriter.addDocument(doc1);
            indexWriter.addDocument(doc2);
            indexWriter.addDocument(doc3);
            indexWriter.commit();
            indexWriter.close();
            directory.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        Date end = new Date();
        System.out.println("索引文档用时: " + (end.getTime() - start.getTime()) + " milliseconds");

    }
}
