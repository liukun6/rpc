package com.lk.search.test;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.highlight.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.wltea.analyzer.lucene.IKAnalyzer;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class HighlighterTest {

    public static void main(String[] args) throws IOException, ParseException, InvalidTokenOffsetsException {
        String field = "title";
        Path indexPath = Paths.get("indexdir");
        Directory dir = FSDirectory.open(indexPath);
        IndexReader reader = DirectoryReader.open(dir);
        IndexSearcher searcher = new IndexSearcher(reader);
        Analyzer analyzer = new IKAnalyzer();
        QueryParser parser = new QueryParser(field, analyzer);
        Query query = parser.parse("北京");
        System.out.println("Query: " + query);
        QueryScorer score = new QueryScorer(query, field);
        //定制高亮标签
        SimpleHTMLFormatter fors = new SimpleHTMLFormatter("<span style=\"color:red; \">","</span>");
        Highlighter highlighter = new Highlighter(fors, score);
        //高亮分词器
        TopDocs tds = searcher.search(query, 10);
        for (ScoreDoc scoreDoc : tds.scoreDocs)
        {
            Document doc = searcher.doc(scoreDoc.doc);
            System.out.println("id: " + doc.get("id"));
            System.out.println("title: " + doc.get("title"));
            TokenStream tokenStream = TokenSources.getAnyTokenStream(searcher.getIndexReader(), scoreDoc.doc, field, analyzer);
            //获取tokenstream
            Fragmenter fragment = new SimpleSpanFragmenter(score);
            highlighter.setTextFragmenter(fragment);
            String str = highlighter.getBestFragment(tokenStream, doc.get(field)); //获取高亮的片段
            System.out.println("高亮的片段:" + str);
        }

        dir.close();
        reader.close();
    }
}
