package com.lk.search.test;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.hunspell.Dictionary;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.wltea.analyzer.lucene.IKAnalyzer;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DeleteIndex {

    public static void main(String[] args) {
        //删除title中含有关键字"美国"的文档
        deleteDoc("title", "美国");
    }

    private static void deleteDoc(String title, String key) {
        Analyzer analyzer = new IKAnalyzer();

        IndexWriterConfig icw = new IndexWriterConfig(analyzer);

        Path indexPath = Paths.get("indexdir");
        Directory dictionary;
        try {
            dictionary = FSDirectory.open(indexPath);
            IndexWriter indexWriter = new IndexWriter(dictionary, icw);
            indexWriter.deleteDocuments(new Term(title, key));

            indexWriter.commit();
            indexWriter.close();
            System.out.println("删除完成!");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
